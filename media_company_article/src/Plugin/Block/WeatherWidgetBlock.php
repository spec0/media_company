<?php

namespace Drupal\media_company_article\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'WeatherWidgetBlock' block.
 *
 * @Block(
 *  id = "weather_widget_block",
 *  admin_label = @Translation("Weather widget block"),
 *  context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *   }
 * )
 */
class WeatherWidgetBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \Drupal\media_company_article\WeatherService
   */
  protected $weatherService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('media_company_article.weather_service');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['block_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Block description'),
      '#default_value' => $this->configuration['block_description'],
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['block_description'] = $form_state->getValue('block_description');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'weather_widget_block';
    $build['#content'][] = $this->configuration['block_description'];

    // Get weather info for the current article based on the post code field
    // of the parent article.
    // Loaded entity is accessible from context injected into the block class
    // from the annotation component
    // context_definitions = {
    // "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
    //}
    $post_code = $this->getContextValue('node')
      ->get('field_post_code')
      ->getString();
    $weather_status = $this->weatherService->getWeatherStatusBasedOnPostCode($post_code);
    $build['#content'][] = 'Weather status in ' . $post_code . ' : ' . $weather_status;

    return $build;
  }

}
