<?php

namespace Drupal\media_company_article;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Strategy\PrivateCacheStrategy;
use Drupal\guzzle_cache\DrupalGuzzleCache;

/**
 * The weather service class.
 */
class WeatherService implements WeatherServiceInterface, ContainerInjectionInterface {

 /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('http_client');
    return $instance;
  }

  /**
   * Make Guzzle client http request to WeatherAPI service.
   * @see https://www.weatherapi.com/docs/
   *
   * @param string $post_code
   *  Post code value from article.
   *
   * @return array
   */
  public function getWeatherStatusBasedOnPostCode(string $post_code) {
    $config = \Drupal::config('madia_company_article.settings');
    $service_hostname = $config->get('service_host_name');
    $api_key = $config->get('api_key');
    $options = [
      'headers' => [
        'Content-Type' => 'application/json',
        'Cache-Control' => 'private, max-age=600',
      ],
      'query' => [
        'key' => $api_key,
        'q' => $post_code,
      ],
    ];

    // Use GuzzleCache if available or fall back to guzzle client otherwise.
    if (\Drupal::service('module_handler')->moduleExists('guzzle_cache')) {
      // Create default HandlerStack
      $stack = HandlerStack::create();
      // Create a Drupal Guzzle cache. Its' useful to have a separate cache bin to
      // manage independent of other cache bins. Here is how you might define such a
      // cache bin in a *.service.yml file:
      // cache.my_custom_http_cache_bin:
      //   class: Drupal\Core\Cache\CacheBackendInterface
      //   tags:
      //     - { name: cache.bin }
      //   factory: cache_factory:get
      //   arguments: [weather_service_cache_bin]
      $cache = new DrupalGuzzleCache(\Drupal::service('cache.weather_service_cache_bin'));
      // Push the cache to the stack.
      $stack->push(
        new CacheMiddleware(
          new PrivateCacheStrategy($cache)
        ),
        'cache'
      );
      // Initialize the client with the handler option
      $client = new Client(['handler' => $stack]);
      $response = $client->request('GET', $service_hostname, $options);
    }
    else {
      $response = $this->httpClient->request('GET', $service_hostname, $options);
    }

    return $this
      ->simplifyWeatherStatus(json_decode($response->getBody()->getContents()));
  }

  /**
   * Simplify weather status based on weather api response data.
   *
   * @param array $data
   *  JSON response from weatherAPI.
   *
   * @return string
   */
  private function simplifyWeatherStatus(array $data) {

    // Here we just trying to simplify the weather status to display.
    if (isset($data['current']['cloud']) && isset($data['current']['precip_mm'])) {

      if ($data['current']['cloud'] < 50 && !$data['current']['precip_mm']) {

        return 'sunny';
      }
      elseif ($data['current']['cloud'] >= 50 && !$data['current']['precip_mm']) {

        return 'cloudy';
      }
      elseif ($data['current']['precip_mm'] > 0) {

        return 'rainy';
      }

    }

  }

}
