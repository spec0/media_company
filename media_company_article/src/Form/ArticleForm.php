<?php

namespace Drupal\media_company_article\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ArticleForm.
 */
class ArticleForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'media_company_article.article',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'article_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('media_company_article.article');
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api key'),
      '#description' => $this->t('WeatherAPI key'),
      '#maxlength' => 255,
      '#size' => 255,
      '#default_value' => $config->get('api_key'),
    ];
    $form['service_host_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service host name'),
      '#description' => $this->t('WeatherAPI endpoint.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('service_host_name'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('media_company_article.article')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('service_host_name', $form_state->getValue('service_host_name'))
      ->save();
  }

}
