# README #

This is Manifesto Logical Test documentation.

### What is this repository for? ###

  * This custom module imeplements all the tasks required for the test.
  * The module has not been fully tested and will probably not work as expected.
  * Example of how we can get all of the features required running on a new drupal installation.

### TASKS ###

  ## TASK 1 - Editorial Experience ##

   * Since the the article content type already exists so we do not need to create new one in the ADMIN UI.
   * I have decided to use Paragraph contrib module to avoid additional custom code in order to implement the requirements for the article components.
   * Basic configuration for the components is exported and added as default config to import after the module is installed based on the config API(config/install folder).
   * Call To Action, Description + Button (link + label), WYSIWYG and Image components are added as separate paragraph types and a field Article content(entity_reference_revisions field type) is added to article page.
   * Please check the article_screenshot.png in the root repository folder for a visual representation.
   * This allows us to move components up and down in the content and the ability to add a component multiple times.
   * Layout builder api might also work but on initial testing it was hard to move created elements around as in requirements.

  ## TASK 2 - Editorial Experience / Permissions ##

   * The fastest way to achieve the requirements is to use a simple module hook as defined in the module .module file. Based on form_id of the article form and the current user role.
   * The description wil only appear for non-admin users. There is a better way based on custom permission possible to achieve this but it is out of the scope of the task.
   * the same effect can also be achieved by TWIG template and variable flags approach.

  ## TASK 3 - Remote API ##

   * For the remote API requirements I have used WheatherAPI public api which docs can be found here: https://www.weatherapi.com/docs/
   * Call to the api like https://api.weatherapi.com/v1/current.json?q=SW1&key=MY_KEY would return something like this:
    {
      "location": {
        "name": "Westminster",
        "region": "London",
        "country": "UK",
        "lat": 51.5,
        "lon": -0.14,
        "tz_id": "Europe/London",
        "localtime_epoch": 1639430815,
        "localtime": "2021-12-13 21:26"
      },
      "current": {
        "last_updated_epoch": 1639430100,
        "last_updated": "2021-12-13 21:15",
        "temp_c": 10,
        "temp_f": 50,
        "is_day": 0,
        "condition": {
          "text": "Overcast",
          "icon": "//cdn.weatherapi.com/weather/64x64/night/122.png",
          "code": 1009
        },
        "wind_mph": 10.5,
        "wind_kph": 16.9,
        "wind_degree": 200,
        "wind_dir": "SSW",
        "pressure_mb": 1025,
        "pressure_in": 30.27,
        "precip_mm": 0,
        "precip_in": 0,
        "humidity": 87,
        "cloud": 75,
        "feelslike_c": 8.4,
        "feelslike_f": 47.2,
        "vis_km": 10,
        "vis_miles": 6,
        "uv": 1,
        "gust_mph": 10.7,
        "gust_kph": 17.3
      }
    }
   * In order to make a request to the api we need api key from a registered account and query param "q" with the post code field from the article passed as a param.
   * The API endpoint and api key are available trough the module configuration form but are not exported here as they would be different for different accounts. Please see src/Form module folder.
   * In order to create the widget I have created a custom block plugin that will display in the page bottom region based on the drupal block layout for simplicity.
   * I have created a custom drupal service implementation which makes the external request based on guzzle http client vendor library in drupal.
   * Adding separate service makes sense in my opinion in order to separate the concerns and follow the SOLID principles as well as not make a mess in the block class if new requirements and features are requested in the future.
   * Guzzle is based ot PSR-7 standard which gives some cool features like immutability and it is based on HTTP messages as in http://tools.ietf.org/html/rfc3986. We can read more about it here: https://www.php-fig.org/psr/psr-7/

  ## TASK 4 - Drupal Backend/Theming ##

   * Post code field is added to article content type trough the admin Ui and is exported as a config and added to the module default confg.
   * Post code needs to be added for every new article in order for the weather widget to work properly so making the field required makes sense here in order to prevent editorial mistakes(or giving it a default value as well).
   * The weather widget is displayed on article page bottom as added in the block configuration.

  ## TASK 5 - "I Feel Good" wildcard ##

   * Regarding caching except for the default drupal cache implementation I have decided to use guzzle_cache contrib module in order to achieve some additional caching techniques. See https://www.drupal.org/project/guzzle_cache
   * For this task I have used cache http header to preserve data from a http request to the weather api for up to 10 mins unless there is a change in the article by editors. 10 mins seems reasonable time for weather changes and we can avoid multiple external http calls when the article is called multiple times in short period like in an event of high traffic and requests to the article.
   * The advantages here are that we are creating an additional cache bin which can be exposed to different cache handlers like redis or memcache(although this is a reccomendation for all cache bins in general).
   * The cache techniques follow the Hypertext Transfer Protocol (HTTP/1.1): Caching which can be found here: https://datatracker.ietf.org/doc/html/rfc7234
   * Please note that the custom cache bin is not complete and correctly implemented it is just to show intentions in the code, I hope it makes sense.

### Who do I talk to? ###

* PS. In general we can also include some custom plugin definitions to make things interesting as described here: https://manifesto.co.uk/drupal-plugin-api-examples-tutorial/ however this would be an overkill for this concrete example(in my opinion).
* To anyone who is checking this documentation I wish you productive day and good luck! :)
